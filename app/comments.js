const express = require('express');
const Comment = require('../models/Comment');
const nanoid = require('nanoid');
const path = require('path');
const router = express.Router();

const config = require('../config');

const createRouter = () => {
    // Comment get

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Comment.findOne({_id: id}, (error, result) => {
            if (error) {
                res.status(400).send(error.message)
            } else {
                res.send(result)
            }
        })
    });

    // Comment post
    router.post('/', (req, res) => {
        const commentData = req.body;

        if(!req.body.description && !req.file) {
            return res.status(400).send('Please fill description or add image');
        }

        const post = new Comment(commentData);

        post.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;