const express = require('express');
const Post = require('../models/Post');
const Comment = require('../models/Comment');
const multer = require('multer');
const nanoid = require('nanoid');
const date = new Date();
const path = require('path');
const router = express.Router();

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    // Post get
    router.get('/', (req, res) => {
        Post.find().sort({date: -1})
            .then(post => res.send(post))
            .catch(() => res.sendStatus(500))

    });

    router.get('/:id', async (req, res) => {
        const id = req.params.id;
        const post = await Post.findOne({_id: id}).populate('User');
        if(post) {
            const comment = await Comment.find({postId: id}).populate('Post');
            res.send({post, comment});
        } else {
            res.status(400).send('Post not found');
        }
    });

    // Post post
    router.post('/', upload.single('image'), (req, res) => {
        const postData = req.body;

        if(!req.body.description && !req.file) {
            return res.status(400).send('Please fill description or add image');
        }

        if (req.file) {
            postData.image = req.file.filename;
        } else {
            postData.image = null;
        }

        postData.date = new Date().toISOString();
        const post = new Post(postData);

        post.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;